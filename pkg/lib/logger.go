package lib

import (
	"fmt"
	"github.com/rs/zerolog"
	"os"
)

var globalLog *Logger

type Logger struct {
	*zerolog.Logger
}

func (l Logger) Println(args ...interface{}) {
	l.Error().Msgf(fmt.Sprint(args...))
}

func GetLogger() *Logger {
	if globalLog != nil {
		return globalLog
	}
	logLevel := zerolog.InfoLevel

	zerolog.SetGlobalLevel(logLevel)

	log := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Timestamp().Logger()

	globalLog = &Logger{&log}

	return globalLog
}
