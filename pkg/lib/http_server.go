package lib

import (
	"github.com/gorilla/handlers"
	"net/http"
	"time"
)

func NewHttpServer(logger *Logger, handler http.Handler, addr string) *http.Server {
	handler = handlers.CompressHandler(handler)

	handler = handlers.LoggingHandler(logger, handler)

	recoveryHandler := handlers.RecoveryHandler(handlers.RecoveryLogger(logger), handlers.PrintRecoveryStack(true))

	server := &http.Server{
		Addr:         ":" + addr,
		Handler:      recoveryHandler(handler),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 50 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	return server
}
