package lib

import (
	"github.com/gorilla/mux"
)

type Router struct {
	*mux.Router
}

type Route interface {
	Setup()
}

func InitializeRouter(logger *Logger) *Router {
	logger.Info().Msg("Setting up Router")

	return &Router{mux.NewRouter()}
}
