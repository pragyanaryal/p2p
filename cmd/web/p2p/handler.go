package p2p

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
	"p2p-system73/internal/p2p"
	"p2p-system73/pkg/lib"
)

type Handler struct {
	logger  *lib.Logger
	service p2p.PService
}

func NewP2PHandler(logger *lib.Logger, service p2p.PService) *Handler {
	return &Handler{logger, service}
}

// JoinNode lets the new node join the p2p network
func (p *Handler) JoinNode(rw http.ResponseWriter, r *http.Request) {
	node := p2p.NodeRequest{}

	err := json.NewDecoder(r.Body).Decode(&node)
	if err != nil {
		p.JSONErrorResponse(rw, err, http.StatusBadRequest)
		return
	}

	joinNode, err := p.service.JoinNode(node)
	if err != nil {
		p.JSONErrorResponse(rw, err, http.StatusInternalServerError)
		return
	}

	p.JSONSuccessResponse(rw, joinNode, http.StatusCreated)
}

// LeaveNode lets the node leave the p2p network, with the given id
func (p *Handler) LeaveNode(rw http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	nodeId, err := uuid.Parse(param["id"])
	if err != nil {
		p.JSONErrorResponse(rw, err, http.StatusBadRequest)
		return
	}

	res, err := p.service.LeaveNode(nodeId)
	if err != nil {
		p.JSONErrorResponse(rw, err, http.StatusNotFound)
		return
	}

	p.JSONSuccessResponse(rw, res, http.StatusOK)
}

// GetAllTrees gets the current status of overall network
func (p *Handler) GetAllTrees(rw http.ResponseWriter, _ *http.Request) {
	p.JSONSuccessResponse(rw, p.service.GetAllTrees(), http.StatusOK)
}

// JSONSuccessResponse sends the json response
func (p *Handler) JSONSuccessResponse(rw http.ResponseWriter, data interface{}, statusCode int) {
	rw.WriteHeader(statusCode)
	rw.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(rw).Encode(map[string]interface{}{"data": data})
}

// JSONErrorResponse sends the error json response
func (p *Handler) JSONErrorResponse(rw http.ResponseWriter, err error, statusCode int) {
	rw.WriteHeader(statusCode)
	rw.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(rw).Encode(map[string]interface{}{"error": err})
}
