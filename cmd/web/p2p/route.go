package p2p

import (
	"github.com/gorilla/mux"
	"net/http"
	"p2p-system73/pkg/lib"
)

type Route struct {
	routes  *mux.Router
	logger  *lib.Logger
	handler *Handler
}

func NewP2PRoute(router *mux.Router, logger *lib.Logger, handler *Handler) *Route {
	return &Route{router, logger, handler}
}

func (p Route) Setup() {
	v1 := p.routes.PathPrefix("/v1").Subrouter()

	p.SetupV1Routes(v1)
}

func (p Route) SetupV1Routes(router *mux.Router) {
	p2route := router.PathPrefix("/p2p").Subrouter()

	p2route.HandleFunc("/join", p.handler.JoinNode).Methods(http.MethodPost)

	p2route.HandleFunc("/leave/{id}", p.handler.LeaveNode).Methods(http.MethodDelete)

	p2route.HandleFunc("/status", p.handler.GetAllTrees).Methods(http.MethodGet)
}
