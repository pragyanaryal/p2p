package docs

import (
	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/mux"
	"net/http"
	"p2p-system73/pkg/lib"
)

// DocRoutes struct
type DocRoutes struct {
	logger *lib.Logger
	routes *mux.Router
}

func NewDocsRoutes(router *mux.Router, logger *lib.Logger) DocRoutes {
	return DocRoutes{
		logger: logger,
		routes: router,
	}
}

// Setup docs routes
func (s DocRoutes) Setup() {
	s.logger.Info().Msg("Setting up Docs routes")

	opts := middleware.SwaggerUIOpts{
		BasePath: "/api/v1/",
		SpecURL:  "docs/openapi.yaml",
		Title:    "P2P Simulation API documentation",
	}

	sh := middleware.SwaggerUI(opts, nil)

	v1 := s.routes.PathPrefix("/v1").Subrouter()

	s.SetupV1Routes(v1, sh)
}

func (s DocRoutes) SetupV1Routes(router *mux.Router, sh http.Handler) {
	p2route := router.PathPrefix("/docs").Subrouter()

	// Routes for Docs
	p2route.Handle("", sh).Methods(http.MethodGet)

	h := http.StripPrefix("/api/v1/docs/", http.FileServer(http.Dir("./docs/")))

	p2route.PathPrefix("/").Handler(h)
}
