package bootstrap

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"p2p-system73/cmd/web/docs"
	"p2p-system73/cmd/web/p2p"
	"p2p-system73/pkg/lib"
)

func InitializeRoutes(logger *lib.Logger, router *lib.Router, handler *Handler) *mux.Router {
	logger.Info().Msg("Setting up routes")

	api := router.PathPrefix("/api").Subrouter()

	RegisterHealthCheck(api, logger)

	routes := []lib.Route{
		p2p.NewP2PRoute(api, logger, handler.P2PHandler),
		docs.NewDocsRoutes(api, logger),
	}

	for _, route := range routes {
		route.Setup()
	}

	return router.Router
}

func RegisterHealthCheck(router *mux.Router, logger *lib.Logger) {
	logger.Info().Msg("Setting up health-check routes")

	router.HandleFunc(
		"/health-check", func(rw http.ResponseWriter, r *http.Request) {
			rw.WriteHeader(http.StatusOK)
			rw.Header().Set("Content-Type", "application/json")
			_ = json.NewEncoder(rw).Encode(
				&struct {
					Data string `json:"data"`
				}{Data: "P2P server up and running"},
			)
		},
	).Methods(http.MethodGet)
}
