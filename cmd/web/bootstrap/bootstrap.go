package bootstrap

import (
	"github.com/joho/godotenv"
	"p2p-system73/internal"
	"p2p-system73/internal/lib"
	lib2 "p2p-system73/pkg/lib"
)

func StartHttpServer() {
	_ = godotenv.Load()

	library := lib.Initialize()

	router := lib2.InitializeRouter(library.Logger)

	repos := internal.InitializeRepos(library.Logger)

	service := internal.InitializeServices(library.Logger, repos)

	handler := InitializeHandlers(library.Logger, service)

	routes := InitializeRoutes(library.Logger, router, handler)

	server := lib2.NewHttpServer(library.Logger, routes, library.Env.ServerPort)

	library.Logger.Info().Msgf("Running server in port %s", library.Env.ServerPort)

	library.Logger.Fatal().Err(server.ListenAndServe()).Msg("Fatal Server Error")

}
