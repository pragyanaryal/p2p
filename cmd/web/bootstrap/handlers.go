package bootstrap

import (
	"p2p-system73/cmd/web/p2p"
	"p2p-system73/internal"
	"p2p-system73/pkg/lib"
)

type Handler struct {
	P2PHandler *p2p.Handler
}

func InitializeHandlers(logger *lib.Logger, services *internal.Services) *Handler {
	logger.Info().Msg("Setting up handlers")

	p2p := p2p.NewP2PHandler(logger, services.P2Service)

	return &Handler{p2p}
}
