package configs

import (
	"github.com/spf13/viper"
	"p2p-system73/pkg/lib"
)

var globalEnv = Env{}

type Env struct {
	ServerPort string `mapstructure:"SERVER_PORT"`
}

func NewEnv(logger *lib.Logger) *Env {
	viper.SetConfigFile(".env")

	err := viper.ReadInConfig()
	if err != nil {
		logger.Fatal().Msgf("cannot read configuration", err)
	}

	viper.SetDefault("SERVER_PORT", "5500")

	err = viper.Unmarshal(&globalEnv)
	if err != nil {
		logger.Fatal().Msgf("environment cant be loaded: ", err)
	}

	return &globalEnv
}
