FROM golang:alpine

RUN apk add build-base

RUN apk add --no-cache git

RUN echo $GOPATH

WORKDIR /p2p-api

COPY . .

RUN go mod download

CMD go run cmd/web/main.go