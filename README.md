# P2P Simulation (In Memory)

This is a piece of program which simulates the p2p connection(join, leave) in memory.

To run this program in your system:
* You need to have `go` installed 
* The version of `go` used here is `1.16`
* Create `.env` file at the root of project.
  * You can set `SERVER_PORT` value if you wish to change the default port
  * Default port will be `9000`


* `go mod download`


* `go run cmd/web/main.go`

If you want to use with docker:

* `docker-compose up`

## P2P Routes
There are 3 REST API routes available to communicate with system.

1. `$base_url/api/v1/join` -->  POST  --> Join the network
2. `$base_url/api/v1/leave` -> DELETE --> Leave the network
3. `$base_url/api/v1/status` -> GET --> Get the current status of network

### Some assumptions made while developing this application

1. New Nodes will get connected to the previous nodes with most free capacity regardless of which tree they are in.
2. New Nodes will start a tree if no node with free capacity is available
3. Upon deleting node, the tree it belongs to will be disabled, thus no new node can get attached to any of its child
4. Upon deleting node, if the existing tree cannot hold entire nodes then remaining nodes will be connected to 
   1. Any other trees nodes based on free capacity 
   2. Or starts new tree
