package internal

import (
	"p2p-system73/internal/p2p"
	"p2p-system73/internal/p2p/memory_repo"
	"p2p-system73/pkg/lib"
)

type Repos struct {
	P2Repo p2p.PRepo
}

func InitializeRepos(logger *lib.Logger) *Repos {
	logger.Info().Msg("Setting up Repo")

	p2 := memory_repo.NewPRepo(logger)

	return &Repos{p2}
}
