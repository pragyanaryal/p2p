package p2p

import (
	"github.com/google/uuid"
)

type PService interface {
	JoinNode(request NodeRequest) (*SingleNodeResponse, error)

	LeaveNode(id uuid.UUID) (*DeleteResponse, error)

	GetAllTrees() []TreeResponse
}

type PRepo interface {
	Insert(request NodeRequest) *SingleNodeResponse

	Leave(id uuid.UUID) (*DeleteResponse, error)

	Print() []TreeResponse
}
