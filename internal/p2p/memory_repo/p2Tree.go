package memory_repo

import (
	"errors"
	"github.com/google/uuid"
	"p2p-system73/internal/p2p"
	"sync"
)

// SingleTree Type declared to hold the unique new SingleTree
type SingleTree struct {
	id       uuid.UUID
	start    *node
	disabled bool
}

// Tree Type declared to hold a SingleTree
type Tree struct {
	// Mutex to avoid data racing
	mu sync.RWMutex

	// Holds the multiple SingleTree
	trees map[uuid.UUID]SingleTree

	// Holds all the nodes information
	nodes map[uuid.UUID]*node

	// Holds all the node with free capacity greater than 0 in sorted way.
	sortedNode sortedNode
}

// Insertion at SingleTree level
// Finding the SingleTree or Node with most free capacity to insert a node on
func (t *Tree) insert(n *node) {
	t.mu.Lock()
	// Check the length of sorted nodes
	// If the length equals to 0, this means no nodes are available to get connected to
	// Insert the node as the new p2p_tree
	if len(t.sortedNode) == 0 {
		t.startTree(n)
	}

	// Insert the new node in sorted node
	s, err := t.sortedNode.insert(n)
	// If the sortedNode cannot find the node to connect to
	// Start the new tree with the given node
	if err != nil {
		t.startTree(n)
	} else {
		// In the case it finds the node
		// Save the updated list of nodes in global variable for Tree data type
		t.sortedNode = s
	}

	// Add all nodes to map to easily locate them
	t.nodes[n.id] = n

	t.mu.Unlock()
}

// start a new tree
func (t *Tree) startTree(n *node) {
	id := uuid.New()
	tree := SingleTree{
		id:       id,
		start:    n,
		disabled: false,
	}
	n.tree = &tree
	t.trees[id] = tree
}

func (t *Tree) findNode(id uuid.UUID) (*node, error) {
	t.mu.RLock()
	defer t.mu.RUnlock()
	if val, ok := t.nodes[id]; ok {
		return val, nil
	}
	return nil, errors.New("node not found")
}

func (t *Tree) deleteNode(id uuid.UUID) (*p2p.DeleteResponse, error) {
	// find the node to delete
	nd, err := t.findNode(id)
	if err != nil {
		return nil, err
	}

	var d p2p.DeleteResponse

	// Get the tree id of the parent node
	tree := nd.tree

	d.TreeId = tree.id

	// Disable the true from attaching new nodes for a while
	tree.disabled = true

	// Find all the nodes within a tree
	// omitting the node that is to be deleted
	nodes := tree.findAllNodesWithinTree(&nd.id, true)

	if len(nodes) > 0 {
		// Merge the nodes in descending order of capacity
		nodes = nodes.mergeSort()

		// Clear all the nodes' association with parent and children and tree
		nodes = nodes.clearParentChildren()

		// The re-build given tree re-arranges all the nodes as much as possible
		// and returns all the nodes that could not be placed within a tree
		remaining := tree.reBuildGivenTree(nodes)

		// Associate the new node as the tree's root node
		t.trees[tree.id] = struct {
			id       uuid.UUID
			start    *node
			disabled bool
		}{id: tree.id, start: tree.start, disabled: false}

		// Insert all the remaining nodes within trees
		// If the available node from any given tree is not available, new tree will be started
		if len(remaining) > 0 {
			var deleted []p2p.LeftNodes
			for _, val := range remaining {
				t.insert(val)
				del := p2p.LeftNodes{
					NodeName:  val.name,
					NewTreeId: &val.tree.id,
				}
				if val.parent != nil {
					del.NewParent = &p2p.Parent{
						Name: val.parent.name,
						Id:   val.parent.id,
					}
				}
				deleted = append(deleted, del)
			}
			d.LeftNodes = deleted
		}
		d.TreeDeleted = false
		d.SortedNetwork = &tree.printTree().Start

		// delete the node from the list of nodes
		delete(t.nodes, nd.id)
	} else {
		delete(t.trees, tree.id)
		d.TreeDeleted = true
	}

	// delete the node from the sorted list of nodes
	t.sortedNode = t.sortedNode.deleteNode(nd)

	// Since the nodes were re-arranged its free capacity has changed, thus
	// Re-arrange the sorted nodes
	t.sortedNode.mergeSort()

	// Remove the nodes with zero free capacity (if any) from sortedNode list
	t.sortedNode = t.sortedNode.removeNodesWithZeroFreeCapacity()

	// Enable the true from attaching new nodes
	tree.disabled = false

	return &d, nil
}

func (s *SingleTree) findAllNodesWithinTree(id *uuid.UUID, omit bool) unsortedNode {
	if omit {
		return s.start.getChildren(id, true)
	}

	return s.start.getChildren(nil, false)
}

func (s *SingleTree) reBuildGivenTree(node unsortedNode) []*node {
	s.start = node[0]
	node[0].parent = nil
	node[0].tree = s

	done := false
	index := 0
	nodes := node[index+1:]

	for !done {
		nodes, done = node[index].insertChildren(nodes)

		index++
	}

	return nodes
}

func (t *Tree) printTree() []p2p.TreeResponse {
	var trees []p2p.TreeResponse
	for _, val := range t.trees {
		trees = append(
			trees, p2p.TreeResponse{
				Id: val.id,
				Start: p2p.NodeResponse{
					Name:              val.start.name,
					Id:                val.start.id,
					Capacity:          val.start.capacity,
					RemainingCapacity: val.start.remainingCapacity,
					Child:             val.start.children.printChildren(),
				},
			},
		)
	}
	return trees
}

func (s *SingleTree) printTree() *p2p.TreeResponse {
	trees := p2p.TreeResponse{
		Id: s.id,
		Start: p2p.NodeResponse{
			Name:              s.start.name,
			Id:                s.start.id,
			Capacity:          s.start.capacity,
			RemainingCapacity: s.start.remainingCapacity,
			Child:             s.start.children.printChildren(),
		},
	}

	return &trees
}
