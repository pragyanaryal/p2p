package memory_repo

import (
    "p2p-system73/internal/p2p"
)

// Type declared to hold a children of a node
type children []*node

func (c *children) printChildren() []p2p.NodeResponse {
    var child []p2p.NodeResponse
    for _, val := range *c {
	  child = append(
		child, p2p.NodeResponse{
		    Name:              val.name,
		    Id:                val.id,
		    Capacity:          val.capacity,
		    RemainingCapacity: val.remainingCapacity,
		    Child:             val.children.printChildren(),
		},
	  )
    }
    return child
}

func (c *children) findChildren() {

}
