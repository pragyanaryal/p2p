package memory_repo

import (
	"errors"
)

// Type declared to store a stored node by remaining capacity.
// Only nodes with capacity greater than 0 will be stored
type sortedNode []*node

func (t sortedNode) insert(n *node) (sortedNode, error) {
	s := t

	if len(s) > 0 {
		// If the sorted array is not empty,
		// then first element of sorted array would be the node with most free capacity
		// hence add the new node as a child of that node
		// But there may be a case when a tree is undergoing re-building phase
		// In that case get next child
		nd := t.getNodeToConnect()

		if nd == nil {
			return nil, errors.New("no node found to connect to")
		}
		nd.insertChild(n)

		// Reset the sortedNode if the current parent ran out of capacity
		if s[0].remainingCapacity == 0 {
			s = s[1:]
		}
	}

	// Sort the array upon insertion, once new node gets attached to an earlier node, its capacity changes
	// hence sort the nodes again
	s = s.sortAllNodesByRemainingCapacity()

	if n.remainingCapacity > 0 {
		if len(s) > 0 {
			s = s.searchAndInsert(n)
		} else {
			s = append(s, n)
		}
	}

	return s, nil
}

func (t sortedNode) getNodeToConnect() *node {

	for _, val := range t {
		if !val.tree.disabled {
			return val
		}
	}
	return nil
}

// Sort the nodes in accordance to remaining capacity
func (t sortedNode) sortAllNodesByRemainingCapacity() sortedNode {
	s := t

	// As the array is mostly sorted
	// And the new node is only attached to the first elements
	// We can rearrange the nodes by checking the capacity of first few elements
	// Insertion sort
	for i := 0; i < len(s); i++ {
		if i+1 < len(s) && s[i].remainingCapacity < s[i+1].remainingCapacity {
			temp := s[i]
			s[i] = s[i+1]
			s[i+1] = temp
		} else {
			// If the next element is not greater than this element,
			// we can break out of the loop and finish sorting
			break
		}
	}

	return s
}

// searchAndInsert searches the array to find the place to insert new node
// this search and insert uses the concept of binary search to find the suitable index to insert the new data
// binary search
func (t sortedNode) searchAndInsert(n *node) sortedNode {
	low, high := 0, len(t)-1
	capacity := n.remainingCapacity
	index := 0

	for low <= high {
		mid := low + (high-low)/2

		if capacity > t[mid].remainingCapacity {
			if len(t[low:high]) == 0 {
				index = mid
				break
			} else if len(t[low:high]) == 1 {
				high = mid
				continue
			}
			high = mid - 1
		} else if capacity <= t[mid].remainingCapacity {
			if len(t[low:high]) == 0 {
				index = mid + 1
				break
			}
			low = mid + 1
		}
	}

	s := t
	s = append(s, n)
	copy(s[index+1:], s[index:])
	s[index] = n

	return s
}

// deletes the provided node from the sorted list of nodes
// uses binary search algorithm to find the index of the node to be deleted from
func (t sortedNode) deleteNode(n *node) sortedNode {

	// this operation needs to be called only if the node has free capacity
	if n.remainingCapacity == 0 {
		return t
	}

	low, high := 0, len(t)-1
	capacity := n.remainingCapacity
	index := 0

	for low <= high {
		mid := low + (high-low)/2

		// If the mid-value is equal to the node to be deleted, it is our index
		if t[mid] == n {
			index = mid
			break
		} else { // In the case of not match to previous condition
			// Since array is sorted in accordance to remaining capacity

			// reduce the search area
			if t[mid].remainingCapacity < capacity {
				// If the capacity of mid-node is less we change the lower boundary to 1 more than of mid
				low = mid + 1
			} else if t[mid].remainingCapacity > capacity {
				// If the capacity of mid-node is more we change the higher boundary to 1 less than of mid
				high = mid - 1
			} else {
				// There may be condition when multiple value of same remaining capacity existing in array
				// In that case we look into the created date of each node
				// Since new node with the same remaining capacity is always bound to be at the bottom of the
				// list

				if t[mid].createdAt > n.createdAt {
					// If the created date of the mid-node is more than current one we change the higher boundary
					high = mid - 1
				} else {
					// If the created date of the mid-node is less than current one we change the higher
					// boundary
					low = mid + 1
				}
			}
		}
	}

	return append(t[:index], t[index+1:]...)
}

func (t sortedNode) mergeSort() sortedNode {
	if len(t) < 2 {
		return t
	}

	return t.merge(t[:len(t)/2].mergeSort(), t[len(t)/2:].mergeSort())
}

func (t sortedNode) merge(l, r sortedNode) sortedNode {
	ret := make(sortedNode, 0, len(l)+len(r))

	for len(l) > 0 || len(r) > 0 {
		if len(l) == 0 {
			return append(ret, r...)
		}
		if len(r) == 0 {
			return append(ret, l...)
		}
		if l[0].capacity >= r[0].capacity {
			ret = append(ret, l[0])
			l = l[1:]
		} else {
			ret = append(ret, r[0])
			r = r[1:]
		}
	}
	return ret
}

func (t sortedNode) removeNodesWithZeroFreeCapacity() sortedNode {
	s := t

	for index := len(s) - 1; index >= 0; index-- {
		if s[index].remainingCapacity == 0 {
			s = s[0 : index-1]
		} else {
			break
		}
	}

	return s
}
