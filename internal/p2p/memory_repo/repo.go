package memory_repo

import (
	"github.com/google/uuid"
	"p2p-system73/internal/p2p"
	"p2p-system73/pkg/lib"
	"time"
)

type PRepo struct {
	logger *lib.Logger
	p2tree *Tree
}

func NewPRepo(logger *lib.Logger) *PRepo {
	p2tree := Tree{
		trees:      make(map[uuid.UUID]SingleTree, 0),
		sortedNode: make(sortedNode, 0),
		nodes:      make(map[uuid.UUID]*node),
	}

	return &PRepo{logger, &p2tree}
}

func (p *PRepo) Insert(request p2p.NodeRequest) *p2p.SingleNodeResponse {
	n := &node{
		id:                uuid.New(),
		name:              request.Name,
		parent:            nil,
		children:          nil,
		createdAt:         time.Now().UnixNano(),
		capacity:          request.Capacity,
		remainingCapacity: request.Capacity,
	}

	p.p2tree.insert(n)

	res := p2p.SingleNodeResponse{
		Name:     n.name,
		Id:       n.id,
		Capacity: n.capacity,
	}

	if n.parent != nil {
		res.Parent = &p2p.Parent{
			Name: n.parent.name,
			Id:   n.parent.id,
		}
	}

	return &res
}

func (p *PRepo) Leave(id uuid.UUID) (*p2p.DeleteResponse, error) {
	return p.p2tree.deleteNode(id)
}

func (p *PRepo) Print() []p2p.TreeResponse {
	return p.p2tree.printTree()
}
