package memory_repo

import (
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestTree(t *testing.T) {
	p2tree := &Tree{
		trees:      make(map[uuid.UUID]SingleTree, 0),
		sortedNode: make(sortedNode, 0),
		nodes:      make(map[uuid.UUID]*node),
	}

	nodes := []*node{
		&node{
			id:                uuid.New(),
			name:              "N0",
			createdAt:         time.Now().UnixNano(),
			capacity:          2,
			remainingCapacity: 2,
		},
		&node{
			id:                uuid.New(),
			name:              "N1",
			createdAt:         time.Now().UnixNano(),
			capacity:          3,
			remainingCapacity: 3,
		},
		&node{
			id:                uuid.New(),
			name:              "N2",
			createdAt:         time.Now().UnixNano(),
			capacity:          0,
			remainingCapacity: 0,
		},
		&node{
			id:                uuid.New(),
			name:              "N3",
			createdAt:         time.Now().UnixNano(),
			capacity:          0,
			remainingCapacity: 0,
		},
		&node{
			id:                uuid.New(),
			name:              "N4",
			createdAt:         time.Now().UnixNano(),
			capacity:          0,
			remainingCapacity: 0,
		},
		&node{
			id:                uuid.New(),
			name:              "N5",
			createdAt:         time.Now().UnixNano(),
			capacity:          0,
			remainingCapacity: 0,
		},
		&node{
			id:                uuid.New(),
			name:              "N6",
			createdAt:         time.Now().UnixNano(),
			capacity:          2,
			remainingCapacity: 0,
		},
	}

	t.Run(
		"Insert new node", func(t *testing.T) {
			p2tree.insert(nodes[0])

			assert.Equal(t, nodes[0], nodes[0].tree.start)
		},
	)

	t.Run(
		"Verify new nodes gets connected to previous node N1", func(t *testing.T) {
			p2tree.insert(nodes[1])
			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[1].parent, nodes[0])

			// Assert that free capacity of node N1 decreases by 1
			assert.Equal(t, nodes[0].remainingCapacity, nodes[0].capacity-1)
		},
	)

	t.Run(
		"Verify new nodes gets attached to nodes with higher free capacity", func(t *testing.T) {
			p2tree.insert(nodes[2])
			p2tree.insert(nodes[3])
			p2tree.insert(nodes[4])

			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[2].parent, nodes[1])

			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[3].parent, nodes[1])

			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[4].parent, nodes[1])
		},
	)

	t.Run(
		"Verify once a nodes free capacity finishes new node takes over", func(t *testing.T) {
			p2tree.insert(nodes[5])

			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[5].parent, nodes[0])
		},
	)

	t.Run(
		"Verify once no nodes are available to connect, new tree will be started", func(t *testing.T) {
			p2tree.insert(nodes[6])

			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[6], nodes[6].tree.start)
		},
	)

	t.Run(
		"Verify once node is deleted node with highest capacity becomes root node", func(t *testing.T) {
			// In our case nodes[1] with capacity 3 should be root
			_, err := p2tree.deleteNode(nodes[4].id)
			if err != nil {
				return
			}

			// Assert that new nodes gets attached to previous node
			assert.Equal(t, nodes[1], nodes[1].tree.start)
		},
	)

	t.Run(
		"Verify that when attempt to delete a node which does not exist with arbitrary id, it will throw an error",
		func(t *testing.T) {
			// In our case nodes[1] with capacity 3 should be root
			_, err := p2tree.deleteNode(uuid.New())
			if err != nil {
				assert.Equal(t, errors.New("node not found"), err)
			}
		},
	)
}
