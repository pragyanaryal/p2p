package memory_repo

// Type declared to store a not stored node
// Used while re-arranging the nodes when nodes get deleted
type unsortedNode []*node

func (c unsortedNode) mergeSort() unsortedNode {
	if len(c) < 2 {
		return c
	}

	return c.merge(c[:len(c)/2].mergeSort(), c[len(c)/2:].mergeSort())
}

func (c unsortedNode) merge(l, r unsortedNode) unsortedNode {
	ret := make(unsortedNode, 0, len(l)+len(r))

	for len(l) > 0 || len(r) > 0 {
		if len(l) == 0 {
			return append(ret, r...)
		}
		if len(r) == 0 {
			return append(ret, l...)
		}
		if l[0].capacity >= r[0].capacity {
			ret = append(ret, l[0])
			l = l[1:]
		} else {
			ret = append(ret, r[0])
			r = r[1:]
		}
	}

	return ret
}

// Clears each node with its associated parent and children
func (c unsortedNode) clearParentChildren() unsortedNode {
	for _, val := range c {
		val.parent = nil
		val.tree = nil
		val.remainingCapacity = val.capacity
		val.children = children{}
	}

	return c
}
