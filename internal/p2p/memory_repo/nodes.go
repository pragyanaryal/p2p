package memory_repo

import (
	"fmt"
	"github.com/google/uuid"
	"sync"
)

// Data type of node to hold its parents and its children along with the capacity of the node
type node struct {
	mu                sync.RWMutex
	id                uuid.UUID
	name              string
	parent            *node
	children          children
	createdAt         int64
	tree              *SingleTree
	capacity          uint
	remainingCapacity uint
}

func (n *node) findParentNode() *node {
	p := n
	for {
		if p.parent != nil {
			p = p.parent
			continue
		}
		break
	}
	return p
}

func (n *node) getChildren(id *uuid.UUID, omit bool) unsortedNode {
	n.mu.RLock()
	var c unsortedNode
	if !(omit == true && n.id == *id) {
		c = append(c, n)
	}

	if len(n.children) > 0 {
		for _, val := range n.children {
			c = append(c, val.getChildren(id, omit)...)
		}
	}
	n.mu.RUnlock()
	return c
}

func (n *node) insertChild(c *node) {
	n.mu.Lock()
	if n.remainingCapacity > 0 {
		c.parent = n
		c.tree = n.tree
		n.children = append(n.children, c)
		n.remainingCapacity = n.remainingCapacity - 1
	}
	n.mu.Unlock()
}

func (n *node) insertChildren(c []*node) (unsortedNode, bool) {
	if n.remainingCapacity == 0 {
		return c, true
	}

	i := 0
	for n.remainingCapacity > 0 {
		if len(c[i:]) > 0 {
			n.insertChild(c[i])
			i++
		} else {
			break
		}
	}

	if len(c[i:]) > 0 {
		return c[i:], false
	}

	return c[i:], true
}

func (n *node) print() {
	fmt.Printf("Parent --> %s(%v) and children -->  ", n.name, n.capacity)
	if len(n.children) > 0 {
		for _, v := range n.children {
			fmt.Printf("%s(%v)  ", v.name, v.capacity)
		}
	}
	fmt.Println()
}
