package p2p

import (
	"github.com/google/uuid"
	"p2p-system73/pkg/lib"
)

type P2Service struct {
	logger *lib.Logger
	repo   PRepo
}

func NewP2Service(logger *lib.Logger, repo PRepo) *P2Service {
	return &P2Service{logger, repo}
}

func (p *P2Service) JoinNode(request NodeRequest) (*SingleNodeResponse, error) {
	res := p.repo.Insert(request)

	return res, nil
}

func (p *P2Service) LeaveNode(id uuid.UUID) (*DeleteResponse, error) {
	return p.repo.Leave(id)
}

func (p *P2Service) GetAllTrees() []TreeResponse {
	return p.repo.Print()
}
