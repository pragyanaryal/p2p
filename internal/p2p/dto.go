package p2p

import "github.com/google/uuid"

type NodeRequest struct {
	Name     string `json:"name"`
	Capacity uint   `json:"capacity"`
}

type NodeResponse struct {
	Name              string         `json:"name"`
	Id                uuid.UUID      `json:"id"`
	Capacity          uint           `json:"capacity"`
	RemainingCapacity uint           `json:"remaining_capacity"`
	Child             []NodeResponse `json:"child"`
}

type TreeResponse struct {
	Id    uuid.UUID    `json:"id"`
	Start NodeResponse `json:"root_node"`
}

type SingleNodeResponse struct {
	Name     string    `json:"name"`
	Id       uuid.UUID `json:"id"`
	Capacity uint      `json:"capacity"`
	Parent   *Parent   `json:"parent"`
}

type Parent struct {
	Name string    `json:"name"`
	Id   uuid.UUID `json:"id"`
}

type DeleteResponse struct {
	TreeDeleted   bool          `json:"tree_deleted"`
	TreeId        uuid.UUID     `json:"tree_id"`
	SortedNetwork *NodeResponse `json:"sorted_network"`
	LeftNodes     []LeftNodes   `json:"left_nodes"`
}

type LeftNodes struct {
	NodeName  string     `json:"node_name"`
	NewParent *Parent    `json:"new_parent"`
	NewTreeId *uuid.UUID `json:"tree_id"`
}
