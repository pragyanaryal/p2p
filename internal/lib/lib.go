package lib

import (
	"p2p-system73/configs"
	"p2p-system73/pkg/lib"
)

type Lib struct {
	Logger *lib.Logger
	Env    *configs.Env
}

func Initialize() *Lib {
	logger := lib.GetLogger()
	env := configs.NewEnv(logger)

	return &Lib{logger, env}
}
