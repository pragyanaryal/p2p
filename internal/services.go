package internal

import (
	"p2p-system73/internal/p2p"
	"p2p-system73/pkg/lib"
)

type Services struct {
	P2Service p2p.PService
}

func InitializeServices(logger *lib.Logger, repo *Repos) *Services {
	logger.Info().Msg("Setting up Services")

	p2 := p2p.NewP2Service(logger, repo.P2Repo)

	return &Services{P2Service: p2}
}
