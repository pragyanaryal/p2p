module p2p-system73

go 1.16

require (
	github.com/go-openapi/runtime v0.24.1
	github.com/google/uuid v1.3.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/rs/zerolog v1.27.0
	github.com/spf13/viper v1.12.0
	github.com/stretchr/testify v1.8.0
)
